# Ansible script to be played on containers

Here I used the [rastasheep/ubuntu-sshd](https://hub.docker.com/r/rastasheep/ubuntu-sshd/) container, delivered with only ssh setup.
This test environnement runs on any linux host.
```
CONTAINER ID        IMAGE                    COMMAND               CREATED             STATUS              PORTS                   NAMES
249760e2f795        rastasheep/ubuntu-sshd   "/usr/sbin/sshd -D"   2 hours ago         Up 2 hours          0.0.0.0:32774->22/tcp   test_sshd
```

Then we just made the needed ssh-key exchange to the server & installed manually the minimum packages list
*python* to make ansible able to run commands localy.

## Then we can write a test playbook like this one:
```
---
- import_playbook: includes/prepare.yaml
- import_playbook: includes/docker.yaml
```

##prepare.yml 
```
---
- hosts: localhost
  tasks:
  - name: start docker service 
  - name: load and start the container we wanna use 
  - name: Wait maximum of 300 seconds for ports to be available 
  - name: force docker to accept sshkey
  - name: append public key from host 
  - name: force install of python to enable ansible action to host
  - name: force setup of python
```

## docker.yaml
```
- hosts: docker
  vars:
    passwordadmin: $6$pbE6yznA$AeFIdIOPgNAIPsFHBapFGllkJ0BY1AJwH8Nc9ktp8EegE86ccdOfE/3Z6Jfw24tkSrtO0zd/LPBbHQz0KrfjK0
  tasks:
  - name: Ensure group "adminlabo" exists
  - name: Install vim,python,sudo,git,ansible packages
  - name: Create admin account & passwords
  - name: Set sudo permissions on adminlabo
```

## Then at last, we can run the playbook:
```
bash-4.4$ sudo ANSIBLE_NOCOWS=1 ansible-playbook -i inventory/host.ini testondocker.yaml 

PLAY [localhost] ************************************************************************************************************************************************

TASK [Gathering Facts] ******************************************************************************************************************************************
ok: [127.0.0.1]

TASK [start docker service] *************************************************************************************************************************************
ok: [127.0.0.1]

TASK [load and start the container we wanna use] ****************************************************************************************************************
ok: [127.0.0.1]

TASK [Wait maximum of 300 seconds for ports to be available] ****************************************************************************************************
ok: [127.0.0.1]

TASK [force docker to accept sshkey] ****************************************************************************************************************************
changed: [127.0.0.1]

TASK [append public key from host] ******************************************************************************************************************************
changed: [127.0.0.1]

TASK [force install of python to enable ansible action to host] *************************************************************************************************
changed: [127.0.0.1]

TASK [force setup of python] ************************************************************************************************************************************
changed: [127.0.0.1]

PLAY [docker] ***************************************************************************************************************************************************

TASK [Gathering Facts] ******************************************************************************************************************************************

changed: [172.17.0.2]

TASK [Ensure group "adminlabo" exists] **************************************************************************************************************************
changed: [172.17.0.2]

TASK [Install vim,python,sudo,git,ansible packages] *************************************************************************************************************
changed: [172.17.0.2] => (item=[u'vim', u'python', u'sudo', u'git', u'ansible'])

TASK [Create admin account & passwords] *************************************************************************************************************************
changed: [172.17.0.2]

TASK [Set sudo permissions on adminlabo] ************************************************************************************************************************
changed: [172.17.0.2]

PLAY RECAP ******************************************************************************************************************************************************
127.0.0.1                  : ok=8    changed=4    unreachable=0    failed=0   
172.17.0.2                 : ok=6    changed=5    unreachable=0    failed=0   
```

## Now we can just logon the contener from adminlabo account with ssh
Then you can check sudo permissions are working fine & adminlabo can `sudo su -` to become root
```
sh adminlabo@172.17.0.2
adminlabo@172.17.0.2's password: 
adminlabo@249760e2f795:~$ sudo su -
root@249760e2f795:~# 
root@249760e2f795:~# exit
logout
adminlabo@249760e2f795:~$ exit
logout
Connection to 172.17.0.2 closed.
```

