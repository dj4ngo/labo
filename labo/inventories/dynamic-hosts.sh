#!/bin/bash

#
# Dynamic inventory : take all production environment vars from gitlab-ci and 
# generate an ansible dynamic inventory
# 

##### Env vars
TMP_FILE="$(mktemp -u --tmpdir='/dev/shm')"
environment_file="$(readlink -f $(dirname $0))/${LCV_ENVIRONMENT:-dev}.yml"

##### Functions

function usage() {
	cat <<EOF
USAGE: $0 (--hosts host|--list)
	$0 is based on LCV_ENVIRONMENT environment variable to select environment yaml.
EOF
}


function generate_yaml () {
	eval "cat <<EOF
$(<$environment_file)
EOF
"
}


function get_list () {
	generate_yaml >$TMP_FILE
	niet -f json $TMP_FILE .groups

}

function get_host () {
	generate_yaml >$TMP_FILE
	niet -f json $TMP_FILE .hosts.$1
}

##### Main
case $1 in 
	--list)
		get_list
	;;
	--host)
		if [ "$#" -ne 2 ]; then
			echo "ERROR: Argument missing !"
			usage
			exit 2
		fi
		get_host $2
	;;
	*)
		usage
		exit 1
	;;
esac

